/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2011
 * Stefan Herbrechtsmeier <stefan@herbrechtsmeier.net>
 *
 * Based on Kirkwood support:
 * (C) Copyright 2009
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Prafulla Wadaskar <prafulla@marvell.com>
 */

#ifndef _CONFIG_DNS320_H
#define _CONFIG_DNS320_H

/*
 * High Level Configuration Options (easy to change)
 */
#define CONFIG_FEROCEON_88FR131		/* CPU Core subversion */
#define CONFIG_KW88F6281		/* SOC Name */

#include "mv-common.h"

/* Remove or override few declarations from mv-common.h */

/*
 * Ethernet Driver configuration
 */
#ifdef CONFIG_CMD_NET
#define CONFIG_MVGBE_PORTS		{1, 0} /* enable port 0 only */
#endif

/*
 * Enable GPI0 support
 */
#define CONFIG_KIRKWOOD_GPIO

/*
 * Environment variables configurations
 */

#if defined(CONFIG_SYS_KWD_CONFIG)
#undef CONFIG_SYS_KWD_CONFIG
#define CONFIG_SYS_KWD_CONFIG          $(CONFIG_BOARDDIR)/kwbimage.cfg
#endif

#if defined(CONFIG_SYS_TCLK)
#undef CONFIG_SYS_TCLK
#define CONFIG_SYS_TCLK                        166666667       /* 800Mhz */
#endif

/*
 * Default environment variables
 */

#define CONFIG_EXTRA_ENV_SETTINGS \
	"stdin=serial\0" \
	"stdout=serial\0" \
	"stderr=serial\0" \
	"loadaddr=0x800000\0" \
	"autoload=no\0" \
	"console=ttyS0,115200\0" \
	"mtdparts="CONFIG_MTDPARTS_DEFAULT \
	"optargs=\0" \
	"bootenv=uEnv.txt\0" \
	"importbootenv=echo Importing environment ...; " \
		"env import -t ${loadaddr} ${filesize}\0" \
	"loadbootenv=fatload usb 0 ${loadaddr} ${bootenv}\0" \
	"setbootargs=setenv bootargs console=${console} " \
		"${optargs} " \
		"${mtdparts} " \
		"root=${bootenvroot} " \
		"rootfstype=${bootenvrootfstype}\0" \
	"subbootcmd=run setbootargs; " \
		"if run bootenvloadimage; then " \
			"bootm ${loadaddr};" \
		"fi;\0" \
	"nandroot=ubi0:rootfs ubi.mtd=rootfs\0" \
	"nandrootfstype=ubifs\0" \
	"nandloadimage=nand read ${loadaddr} kernel\0" \
	"setnandbootenv=echo Booting from nand ...; " \
		"setenv bootenvroot ${nandroot}; " \
		"setenv bootenvrootfstype ${nandrootfstype}; " \
		"setenv bootenvloadimage ${nandloadimage}\0"

#define CONFIG_BOOTCOMMAND \
	"if test -n ${bootenv} && usb start; then " \
		"if run loadbootenv; then " \
			"echo Loaded environment ${bootenv} from usb;" \
			"run importbootenv;" \
		"fi;" \
		"if test -n ${bootenvcmd}; then " \
			"echo Running bootenvcmd ...;" \
			"run bootenvcmd;" \
		"fi;" \
	"fi;" \
	"run setnandbootenv subbootcmd;"

#endif /* _CONFIG_DNS320_H */
